package com.example.a24419.myapplication;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import static com.example.a24419.myapplication.GameActivity.height;
import static com.example.a24419.myapplication.GameActivity.width;

/**
 * Created by 24419 on 2018/3/4.
 */

// This class is for the frog
public class Frog extends android.support.v7.widget.AppCompatImageView implements Runnable {
    float xt,yt,start_x,start_y;
    double speed1;
    Handler timer;
    boolean pos;
    boolean landed = true;
    public Frog(Context context, @Nullable AttributeSet attrs,int image_id, double speed, float x, float y){
        super(context,attrs);
        this.setImageResource(image_id);
        timer = new Handler();
        timer.postDelayed(this,10);
        xt = x;
        yt = y;
        this.setX(x);
        this.setY(y);
        speed1 = speed;
        start_x = x;
        start_y=y;
    }

    @Override
    public void run() {
        if (pos){      // moving right
            xt += speed1;
            this.setX(xt);
        }
        else{
            xt-=speed1;   // moving left
            this.setX(xt);
        }
        if (xt>width || xt<0){   // if the frog is out of the window, the frog returns to its staring point
            xt = start_x;
            yt = start_y;
            speed1 = 0;
            this.setX(start_x);
            this.setY(start_y);
            this.setRotation(180);
        }
        this.invalidate();
        timer.postDelayed(this,10);
    }

    public void restart(){
        xt = width/11;
        yt = height /16*13;
        speed1 = 0;
        setX(width/11);
        setY(height /16*13);
        landed = true;
    }
}
