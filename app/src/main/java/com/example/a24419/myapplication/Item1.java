package com.example.a24419.myapplication;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import static com.example.a24419.myapplication.GameActivity.width;

/**
 * Created by 24419 on 2018/3/5.
 */
//  This class is for items that when frog runs into these items, the frog will land

public class Item1 extends CustomView {
    public Item1(Context context, @Nullable AttributeSet attrs, int image_id, double speed, float x, float y, boolean position) {
        super(context, attrs, image_id, speed, x, y, position);
    }

    @Override
    public void run(){
        if (pos){               //  right movement
            xt += speed1;
        }
        else{                 // left movement
            xt-=speed1;
        }
        if (pos && xt>=width){         // reset starting x when the image is out of the window for right movement
            xt =0-this.newbm.getWidth();
        }
        if (!pos && xt < -newbm.getWidth()){  // reset starting x when the image is out of the window for left movement
            xt = width;
        }
        this.invalidate();
        timer.postDelayed(this,10);
    }
}
