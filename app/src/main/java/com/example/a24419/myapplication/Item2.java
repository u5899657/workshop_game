package com.example.a24419.myapplication;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import static com.example.a24419.myapplication.GameActivity.frog;
import static com.example.a24419.myapplication.GameActivity.width;

/**
 * Created by 24419 on 2018/3/5.
 */
//  This class is for vehicles that when frog runs into these items, the frog will die

public class Item2 extends CustomView {
    public Item2(Context context, @Nullable AttributeSet attrs, int image_id, double speed, float x, float y, boolean position) {
        super(context, attrs, image_id, speed, x, y, position);
    }

    @Override
    public void run(){
        if (pos){               // right movement
            xt += speed1;
        }
        else{                   // left movement
            xt-=speed1;
        }
        if (pos && xt>=this.getWidth()){   // reset starting x when the image is out of the window for right movement
            xt = 0-this.newbm.getWidth();
        }
        if (!pos && xt < -this.newbm.getWidth()){  // reset starting x when the image is out of the window for left movement
            xt = width;
        }
        this.invalidate();
        timer.postDelayed(this,10);

        if (frog.yt ==this.yt ){           //  check whether the frog runs into the item, if true, the frog will die and return to its original starting point
            if (frog.xt+frog.getWidth()>xt && frog.xt<xt+newbm.getWidth()){
                frog.restart();
            }
        }
    }
}
