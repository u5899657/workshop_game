package com.example.a24419.myapplication;

import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends AppCompatActivity implements View.OnTouchListener {
    int score = 0;
    float current_y;
    AnimationDrawable animationDrawable;
    List<Integer>destination_x = new ArrayList<>();
    List<Integer>destination_y = new ArrayList<>();
    int[] anim = new int[]{R.drawable.frog1,R.drawable.frog2,R.drawable.frog3,R.drawable.frog4,R.drawable.frog5,R.drawable.frog6,R.drawable.frog7};
    RelativeLayout myLayout;
    MyView myview;
    public static Frog frog;
    public static int height;
    public static int width;
    List<Item1> items = new ArrayList<>();
    ImageView up,down,left,right;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        WindowManager wm1 = this.getWindowManager();
        width = wm1.getDefaultDisplay().getWidth();         // get the width of the screen
        height = wm1.getDefaultDisplay().getHeight();      // get the height of the screen
        current_y=height /16*13;
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();                      //  Get rid of the title bar
        setContentView(R.layout.activity_game);
        myLayout = findViewById(R.id.game_layout);
        Item1 car = new Item1(this,null,R.drawable.car1,2.0d,0, height /16*3,true);
        Item1 wood = new Item1(this,null,R.drawable.wood,1.0d,width, height /16*4,false);
        Item1 turtle = new Item1(this,null,R.drawable.turtle,3.0d,0, height /16*5,true);
        createItem1(3,R.drawable.turtle,3.0d,0, height /16*7,true,turtle.newbm.getWidth());
        createItem1(2,R.drawable.turtle,2.0d,0, height /16*3,true,turtle.newbm.getWidth());
        createItem1(3,R.drawable.turtle,2.0d,5*turtle.newbm.getWidth(), height /16*3,true,turtle.newbm.getWidth());
        createItem1(1,R.drawable.wood,2.0d,width, height /16*4,false,wood.newbm.getWidth());
        createItem1(3,R.drawable.turtle,3.0d,0, height /16*5,true,turtle.newbm.getWidth());
        createItem1(1,R.drawable.wood,2.0d,width, height /16*6,false,wood.newbm.getWidth());
        createItem2(2,R.drawable.car1,4.0d,0, height /16*9,true,car.newbm.getWidth());
        createItem2(2,R.drawable.car1,4.0d,width, height /16*10,false,car.newbm.getWidth());
        createItem2(2,R.drawable.car1,4.0d,car.newbm.getWidth()*2, height /16*9,true,car.newbm.getWidth());
        createItem2(2,R.drawable.car1,4.0d,0, height /16*11,true,car.newbm.getWidth());
        createItem2(2,R.drawable.car1,4.0d,width, height /16*12,false,car.newbm.getWidth());
        myview = new MyView(this,null,score);
        frog = new Frog(this,null,R.drawable.frog1,0,width/11, height /16*13);
        frog.setRotation(180);
        frog.setImageResource(R.drawable.anim);
        myLayout.addView(frog);
        myLayout.addView(myview);
        myLayout.setOnTouchListener(this);
        destination_x.add(width/11*2);
        destination_y.add(height /16*2);
        destination_x.add(width/11*5);
        destination_y.add(height /16*2);
        destination_x.add(width/11*8);
        destination_y.add(height /16*2);
        init();
    }

    public void init() {
        up = new ImageView(this);
        up.setImageResource(R.drawable.rightpic);
        up.setRotation(270);
        up.setScaleX(3);
        up.setScaleY(3);
        up.setAlpha(0.7f);
        up.setX(width / 2);
        up.setY(height / 2);
        myLayout.addView(up);
        up.setOnTouchListener(this);
        down = new ImageView(this);
        down.setImageResource(R.drawable.rightpic);
        down.setRotation(90);
        down.setScaleX(3);
        down.setScaleY(3);
        down.setAlpha(0.7f);
        down.setX(width / 2);
        down.setY(height / 6 * 5);
        myLayout.addView(down);
        down.setOnTouchListener(this);
        right = new ImageView(this);
        right.setImageResource(R.drawable.rightpic);
        right.setScaleX(3);
        right.setScaleY(3);
        right.setAlpha(0.7f);
        right.setX(width / 4*3);
        right.setY(height / 6 * 4);
        myLayout.addView(right);
        right.setOnTouchListener(this);
        left = new ImageView(this);
        left.setImageResource(R.drawable.rightpic);
        left.setScaleX(3);
        left.setScaleY(3);
        left.setRotation(180);
        left.setAlpha(0.7f);
        left.setX(width / 4);
        left.setY(height / 6 * 4);
        myLayout.addView(left);
        left.setOnTouchListener(this);
    }

    public void stopAnim(){    //  Stop the animation for jumping of the frog, the animation last for 350 ms
        TimerTask task1= new TimerTask() {
            @Override
            public void run() {
                /**
                 *要执行的操作
                 */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        frog.setImageResource(R.drawable.anim);
                    }
                });

            }
        };
        Timer timer1 = new Timer();
        timer1.schedule(task1, 350);
    }

    public void createItem1(int num,int id,double speed,float x, float y,boolean pos,int width){    // create items which can be landed by the frog
        if (pos) {
            for (int i = 0; i < num; i++) {           //  for moving right
                Item1 turtle1 = new Item1(this, null, id, speed, x+(i - num +1) * width, y, pos);
                items.add(turtle1);
                myLayout.addView(turtle1);
            }
        }

        else {
            for (int i = 0; i < num; i++) {       // for moving left
                Item1 turtle1 = new Item1(this, null, id, speed, x+ i * width, y, pos);
                items.add(turtle1);
                myLayout.addView(turtle1);
            }
        }
    }

    public void createItem2(int num,int id,double speed,float x, float y,boolean pos,int width){   // create vehicles
        if (pos) {
            for (int i = 0; i < num; i++) {    // for moving right
                Item2 item = new Item2(this, null, id, speed, x+(i - num +1) * width, y, pos);
                myLayout.addView(item);
            }
        }

        else {
            for (int i = 0; i < num; i++) {    // for moving left
                Item2 item = new Item2(this, null, id, speed, x+ i * width, y, pos);
                myLayout.addView(item);
            }
        }
    }

    public void add_score(){
        System.out.println(current_y);
        if (frog.yt<current_y){
            score += 10;
            current_y = frog.yt;
            myview.updateScore(score);
        }
    }

    @Override
        public boolean onTouch(View v, final MotionEvent event){     // Control of the frog

            if (event.getAction() == MotionEvent.ACTION_DOWN){      //  when the screen is touched
                animationDrawable =(AnimationDrawable) frog.getDrawable();
                        if (v.equals(down)) {           // frog moving down
                            animationDrawable.start();
                            frog.setRotation(0);
                            frog.yt += height / 16;
                            frog.setY(frog.yt);
                            stopAnim();
                        }
                        else if(event.getY()< height /7*5 && v.equals(up)){       // frog moving up
                            animationDrawable.start();
                            frog.yt -= height / 16;
                            frog.setY(frog.yt);
                            frog.setRotation(180);
                            stopAnim();
                        }
                        else if(v.equals(left)){    // frog moving left
                            animationDrawable.start();
                            frog.xt -= width / 11;
                            frog.setX(frog.xt);
                            frog.setRotation(90);
                            stopAnim();
                        }
                        else if(v.equals(right)){    // frog moving right
                            animationDrawable.start();
                            frog.xt += width / 11;
                            frog.setX(frog.xt);
                            frog.setRotation(270);
                            stopAnim();
                        }

                for (int i = 0;i<3;i++){           // Check whether the frog jumps into one of the destinations
                    if (frog.xt>destination_x.get(i)-frog.getWidth()/2 && frog.xt<destination_x.get(i)+frog.getWidth()/2&& frog.yt ==destination_y.get(i)){
                        System.out.println(i);
                        ImageView im = new ImageView(this);
                        im.setImageResource(R.drawable.destination);
                        im.setScaleX(0.5f);
                        im.setScaleY(0.5f);
                        im.setX(width/15*(2*i+1)*2);
                        im.setY(height/16+20);
                        myLayout.addView(im);

                    }
                }
                if (frog.yt <= height /16*7) {     // Check whether the frog lands on one of the items
                    for (Item1 view : items) {
                        if (frog.yt == view.yt && frog.xt > view.xt-frog.getWidth()/2 && frog.xt +frog.getWidth()/2< view.xt + view.newbm.getWidth()) {
                            frog.speed1 = view.speed1;                      // if landed, the frog moves with the speed of the item
                            frog.pos = view.pos;
                            frog.landed = true;
                            add_score();
                            break;
                        }
                        else {
                            frog.landed = false;
                        }
                    }
                }

                else {
                    add_score();
                    frog.speed1 = 0;
                }

                if (!frog.landed){               //  if the frog is not landing on item or the ground, the frog will die and return to its starting point
                    frog.restart();
                }

            }




            return true;
        }
}
