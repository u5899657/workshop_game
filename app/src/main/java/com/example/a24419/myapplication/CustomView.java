package com.example.a24419.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by 24419 on 2018/3/2.
 */

public class CustomView extends View implements Runnable {
    float xt = 0.0f;      // moving_x
    float yt = 0.0f;      // moving_y
    float start_x;
    Handler timer;
    Bitmap myImage,newbm;
    double speed1;      // the speed of movement
    boolean pos;        // true for right movement, false for left movement


    public CustomView(Context context, @Nullable AttributeSet attrs, int image_id, double speed, float x, float y,boolean position) {
        super(context, attrs);
        Drawable drawable = getResources().getDrawable(image_id);
        myImage = ((BitmapDrawable) drawable).getBitmap();
        int width = myImage.getWidth();
        int height = myImage.getHeight();
        // 设置想要的大小
        double newWidth = width*0.6;
        double newHeight = height*0.6;
        // 计算缩放比例
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片
        newbm = Bitmap.createBitmap(myImage, 0, 0, width, height, matrix,
                true);
        timer = new Handler();
        timer.postDelayed(this,10);
 //       this.setOnTouchListener(this);
        speed1 = speed;
        if (position) {                        //  set starting x coordinate
            start_x = x - newbm.getWidth();
        }
        else {
            start_x = x;
        }
        pos = position;          // set movement
        xt = start_x;
        yt = y;


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p = new Paint();
        p.setColor(Color.BLUE);
        p.setStrokeWidth(10.0f);
        canvas.drawBitmap(newbm ,xt, yt, p);
    }

  //  @Override
  //  public boolean onTouch(View v, MotionEvent event) {
   //     if (event.getAction() == MotionEvent.ACTION_DOWN) {
   //         xt = event.getX();
   //         yt = event.getY();
   //         this.invalidate();
   //     }
   //     return true;
   // }


    @Override
    public void run() {

    }
}
